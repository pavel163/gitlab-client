package ru.terrakok.gitlabclient.toothpick.qualifier

import javax.inject.Qualifier

/**
 * @author Eugene Shapovalov (CraggyHaggy). Date: 29.09.17
 */
@Qualifier
annotation class TodoListPendingState